import { FC } from "react";
import { PaginationLink } from "./PaginationLink";
import styles from "./Pagination.module.css";

interface IPaginationProps {
  pages: number;
  currentPage: number;
}

const Pagination: FC<IPaginationProps> = ({ pages, currentPage }) => {
  const prev = currentPage - 1;
  const next = currentPage + 1;

  return (
    <div className={styles.Pagination}>
      <div>
        {prev > 0 && <PaginationLink title="Previous page" number={prev} />}
      </div>

      <div>
        {next <= pages && <PaginationLink title="Next page" number={next} />}
      </div>
    </div>
  );
};

export default Pagination;
