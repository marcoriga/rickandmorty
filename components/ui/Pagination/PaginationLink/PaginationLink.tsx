import { FC } from "react";
import Link from "next/link";

interface IPaginationLinkProps {
  title: string;
  number: number;
}

const PaginationLink: FC<IPaginationLinkProps> = ({ title, number }) => {
  return (
    <Link passHref shallow={true} href={number > 1 ? `/?page=${number}` : ""}>
      <a className="button" title={`${title}`}>
        {title}
      </a>
    </Link>
  );
};

export default PaginationLink;
