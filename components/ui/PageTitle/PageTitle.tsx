import { useRouter } from "next/router";
import { FC } from "react";
import styles from "./PageTitle.module.css";

interface IPageTitleProps {
  back?: boolean;
  title?: string;
}

const PageTitle: FC<IPageTitleProps> = ({
  back = false,
  title = process.env.NEXT_PUBLIC_SITE_TITLE,
}) => {
  const router = useRouter();

  return (
    <div className={styles.Container}>
      {back && (
        <button
          className={styles.Button}
          onClick={router.back}
          aria-label="Back to previous page"
        >
          ← Back
        </button>
      )}
      <h1 className={styles.PageTitle}>{title}</h1>
    </div>
  );
};

export default PageTitle;
