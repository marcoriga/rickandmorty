export { default as CharacterCard } from "./CharacterCard";
export { default as EpisodesList } from "./EpisodesList";
export { default as Informations } from "./Informations";
export { default as List } from "./List";
export { default as Location } from "./Location";
export { default as Thumb } from "./Thumb";
