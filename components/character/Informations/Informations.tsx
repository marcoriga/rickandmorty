import { FC } from "react";
import { Gender, Status } from "@defs";
import styles from "./Informations.module.css";

interface IInformationsProps {
  name: string;
  gender: Gender;
  species: string;
  status: Status;
}

const Informations: FC<IInformationsProps> = ({
  name,
  gender,
  species,
  status,
}) => {
  return (
    <div className={styles.Info}>
      <h3>{name}</h3>

      <div className={styles.Data}>
        {status !== "unknown" && <span>{status}</span>}
        {gender !== "unknown" && <span>{gender}</span>}
        <span>{species}</span>
      </div>
    </div>
  );
};

export default Informations;
