import { FC } from "react";
import Link from "next/link";
import { Informations, Thumb } from "@components/character";
import { CharacterCardType } from "@queries/characters";

import styles from "./CharacterCard.module.css";

interface ICharacterCardProps {
  data: CharacterCardType;
}

const CharacterCard: FC<ICharacterCardProps> = ({ data }) => {
  return (
    <Link shallow={true} passHref href={`/character/${data.id}`}>
      <a title={data.name} className={styles.Character}>
        <div className={styles.Content}>
          <Thumb src={data.image} alt={data.name} />

          <div className={styles.Body}>
            <Informations
              name={data.name}
              gender={data.gender}
              status={data.status}
              species={data.species}
            />
          </div>
        </div>
      </a>
    </Link>
  );
};

export default CharacterCard;
