import { FC } from "react";
import styles from "./EpisodeList.module.css";
import { IEpisode } from "@defs";

interface IEpisodesListProps {
  episodes: Partial<IEpisode>[];
}

const EpisodeList: FC<IEpisodesListProps> = ({ episodes }) => {
  return (
    <div className={styles.Wrapper}>
      <h5>Episodes</h5>

      <div className={styles.Lists}>
        {episodes.map((e) => e.name).join(" - ")}
      </div>
    </div>
  );
};

export default EpisodeList;
