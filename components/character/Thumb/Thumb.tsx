import { FC } from "react";
import styles from "./Thumb.module.css";

interface IThumbProps {
  src: string;
  alt: string;
}

const Thumb: FC<IThumbProps> = ({ src, alt }) => {
  return (
    <div className={styles.Thumb}>
      <div className={styles.ThumbInner}>
        <img src={src} alt={alt} loading="lazy" />
      </div>
    </div>
  );
};

export default Thumb;
