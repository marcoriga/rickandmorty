import { FC } from "react";
import styles from "./List.module.css";

const List: FC = ({ children }) => {
  return <div className={styles.Wrapper}>{children}</div>;
};

export default List;
