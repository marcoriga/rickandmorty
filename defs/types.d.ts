export declare type Gender = "Female" | "Male" | "Genderless" | "unknown";

export declare type Status = "Alive" | "Dead" | "unknown";

/**
 * Episode type
 */
export interface IEpisode {
  id: number;
  name: string;
  air_date: string;
  episode: string;
  characters: string[];
  url: string;
  created: string;
}

/**
 * Location type
 */
export interface ILocation {
  id: number;
  name: string;
  type: string;
  dimension: string;
  residents: Partial<ICharacter>[];
  url: string;
  created: string;
}

/**
 * Character type
 */
export interface ICharacter {
  id: number;
  name: string;
  gender: Gender;
  image: string;
  species: string;
  status: Status;
  type: string;
  origin: Partial<ILocation>;
  location: Partial<ILocation>;
  episode: Partial<IEpisode>[];
  url: string;
  created: string;
}

/**
 * Paginated API resource
 */
export interface IPagination<T> {
  info: {
    pages: number;
  };
  results: T[];
}
