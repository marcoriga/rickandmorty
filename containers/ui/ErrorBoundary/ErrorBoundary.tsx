import { Component, ReactNode } from "react";
import { Container, ErrorWarning, PageTitle } from "@components/ui";

interface Props {
  children: ReactNode;
}

interface State {
  hasError: boolean;
}

class ErrorBoundary extends Component<Props, State> {
  state: State = {
    hasError: false,
  };

  static getDerivedStateFromError() {
    return { hasError: true };
  }

  render() {
    if (this.state.hasError) {
      return (
        <Container>
          <PageTitle />
          <ErrorWarning />
        </Container>
      );
    }

    return this.props.children;
  }
}

export default ErrorBoundary;
