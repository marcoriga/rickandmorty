import { Container, PageTitle } from "@components/ui";
import Link from "next/link";
import styles from "@styles/404.module.css";

export default function Custom404() {
  return (
    <Container>
      <PageTitle title="404 - Not found" />

      <div className={styles.NotFound}>
        <p>We can't find the page you are looking for.</p>

        <Link passHref href="/">
          <a className="button" title="Back to home">
            Back to home
          </a>
        </Link>
      </div>
    </Container>
  );
}
