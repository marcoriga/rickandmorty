import { FC, useEffect } from "react";
import { useRouter } from "next/router";
import type { AppProps } from "next/app";
import { ApolloProvider } from "@apollo/client";
import { useApollo } from "@client";
import "../styles/globals.css";
import { ErrorBoundary } from "@containers/ui";

const App: FC<AppProps> = ({ Component, pageProps }) => {
  const router = useRouter();
  const apolloClient = useApollo(pageProps.initialApolloState);

  useEffect(() => {
    const handleRouteChange = () => {
      window.scroll(0, 0);
    };

    router.events.on("routeChangeComplete", handleRouteChange);

    return () => {
      router.events.off("routeChangeComplete", handleRouteChange);
    };
  }, []);

  return (
    <ApolloProvider client={apolloClient}>
      <ErrorBoundary>
        <Component {...pageProps} />
      </ErrorBoundary>
    </ApolloProvider>
  );
};

export default App;
