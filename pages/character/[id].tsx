import { FC } from "react";
import { useQuery } from "@apollo/client";
import { useRouter } from "next/router";
import { GetServerSideProps } from "next";
import Head from "next/head";
import { initializeApollo } from "@client";
import { EpisodesList, Thumb, Location } from "@components/character";
import { Container, PageTitle, Spinner } from "@components/ui";
import {
  GET_CHARACTER,
  ICharacterQuery,
  ICharacterQueryParams,
} from "@queries/character";
import styles from "@styles/Character.module.css";

const Character: FC = () => {
  const router = useRouter();
  const id: number = parseInt(router.query.id as string);

  const { loading, data } = useQuery<ICharacterQuery, ICharacterQueryParams>(
    GET_CHARACTER,
    {
      variables: { id },
    }
  );

  if (loading) {
    return (
      <Container>
        <Spinner />
      </Container>
    );
  }

  const { name, image, episode, origin, location } = data!.character;

  return (
    <>
      <Head>
        <title>
          {name} - {process.env.NEXT_PUBLIC_SITE_TITLE}
        </title>
      </Head>

      <Container>
        <PageTitle title={name} back={true} />
        <Thumb alt={name} src={image} />

        <div className={styles.Content}>
          {origin.name !== "unknown" && (
            <Location title="Origin" location={origin} />
          )}

          {location.name !== "unknown" && (
            <Location title="Last known location" location={location} />
          )}

          <EpisodesList episodes={episode} />
        </div>
      </Container>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async ({ query }) => {
  try {
    const apolloClient = initializeApollo();
    const id: number = parseInt(query.id as string);

    await apolloClient.query<ICharacterQuery>({
      query: GET_CHARACTER,
      variables: { id },
    });

    return {
      props: {
        initialApolloState: apolloClient.cache.extract(),
      },
    };
  } catch {
    return {
      notFound: true,
    };
  }
};

export default Character;
