import React, { FC } from "react";
import { GetServerSideProps } from "next";
import { useRouter } from "next/router";
import Head from "next/head";
import { useQuery } from "@apollo/client";
import { initializeApollo } from "@client";
import { Container, PageTitle, Pagination, Spinner } from "@components/ui";
import { CharacterCard, List } from "@components/character";
import {
  GET_CHARACTERS,
  ICharactersQuery,
  ICharactersQueryParams,
} from "@queries/characters";

const Home: FC = () => {
  const router = useRouter();
  const page = router.query.page ? parseInt(router.query.page as string) : 1;

  const { loading, data } = useQuery<ICharactersQuery, ICharactersQueryParams>(
    GET_CHARACTERS,
    { variables: { page } }
  );

  return (
    <>
      <Head>
        <title>
          {page > 1 ? `Page ${page} - ` : ""}
          {process.env.NEXT_PUBLIC_SITE_TITLE}
        </title>
      </Head>

      <Container>
        <PageTitle />

        {loading && <Spinner />}

        {!loading && data && (
          <>
            <List>
              {data.characters.results.map((item) => (
                <CharacterCard key={item.id} data={item} />
              ))}
            </List>

            <Pagination pages={data.characters.info.pages} currentPage={page} />
          </>
        )}
      </Container>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async ({ query }) => {
  try {
    const apolloClient = initializeApollo();
    const page: number = query.page ? parseInt(query.page as string) : 1;

    await apolloClient.query<ICharactersQuery>({
      query: GET_CHARACTERS,
      variables: { page },
    });

    return {
      props: {
        initialApolloState: apolloClient.cache.extract(),
      },
    };
  } catch {
    return {
      notFound: true,
    };
  }
};

export default Home;
