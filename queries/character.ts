import { gql } from "@apollo/client";
import { ICharacter } from "@defs";

export declare type CharacterDetailType = Pick<
  ICharacter,
  "id" | "name" | "image" | "origin" | "location" | "episode"
>;

export interface ICharacterQuery {
  character: CharacterDetailType;
}

export interface ICharacterQueryParams {
  id: number;
}

export const GET_CHARACTER = gql`
  query Character($id: ID!) {
    character(id: $id) {
      id
      name
      image
      episode {
        name
      }
      location {
        name
        type
        dimension
        residents {
          name
        }
      }
      origin {
        name
        type
        dimension
        residents {
          name
        }
      }
    }
  }
`;
