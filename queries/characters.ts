import { gql } from "@apollo/client";
import { ICharacter, IPagination } from "@defs";

export declare type CharacterCardType = Pick<
  ICharacter,
  "id" | "name" | "status" | "image" | "gender" | "species"
>;

export interface ICharactersQuery {
  characters: IPagination<CharacterCardType>;
}

export interface ICharactersQueryParams {
  page: number;
}

export const GET_CHARACTERS = gql`
  query Characters($page: Int!) {
    characters(page: $page) {
      info {
        pages
      }
      results {
        id
        name
        status
        image
        gender
        species
      }
    }
  }
`;
